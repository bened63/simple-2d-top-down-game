# Simple 2D Top-Down Game

A simple 2D Top-Down game to start a new 2D game. The following features are basically implemented:
 - Player input and movement
 - Cinemachine
 - Enemy that follows Player
 - Enemy Spawner
 - Main Menu
 - Gameplay UI (Healthbar XPBar, Debug Details)
 - Collectibles
 - Weapon
 - Audio
 - Level Up Screen
 - Upgrades for Player


## Visuals
![](media/video.mp4)  
