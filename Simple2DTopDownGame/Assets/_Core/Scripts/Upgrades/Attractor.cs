using System.Collections;
using UnityEngine;

public class Attractor : MonoBehaviour
{
	[SerializeField] private CircleCollider2D _collider;
	[SerializeField, Min(0f)] private float _radius = 0.5f;

	private Coroutine _radiusCoroutine;

	public float radius
	{
		get => _radius;
		set
		{
			_radius = value;
			//transform.localScale = Vector3.one * _radius;
			if (_radiusCoroutine != null) StopCoroutine(_radiusCoroutine);
			_radiusCoroutine = StartCoroutine(IELerpToRadius());
		}
	}

	private void Start()
	{
		if (_collider == null)
		{
			_collider = GetComponent<CircleCollider2D>();
		}
	}

	private void OnValidate()
	{
		radius = _radius;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Collectible")
		{
			Collectible c = collision.GetComponent<Collectible>();
			c.MoveToPlayer();
		}
	}


	private IEnumerator IELerpToRadius()
	{
		float currentLerpTime = 0f;
		float lerpTime = Mathf.Clamp(1f, 0.0001f, float.PositiveInfinity);
		bool run = true;

		Vector3 start = transform.localScale;
		Vector3 end = Vector3.one * _radius;

		while (run)
		{
			//increment timer once per frame
			currentLerpTime = Mathf.Clamp(currentLerpTime += Time.deltaTime, 0f, lerpTime);

			//lerp!
			float t = currentLerpTime / lerpTime;
			t = Toolbox.Easings.Interpolate(t, Toolbox.Easings.Function.EaseInOutElastic);
			transform.localScale = Vector3.LerpUnclamped(start, end, t);

			// finish
			run = currentLerpTime < lerpTime;

			yield return null;
		}


	}

}