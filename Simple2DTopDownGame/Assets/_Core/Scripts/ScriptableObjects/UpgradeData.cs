﻿using UnityEngine;

[CreateAssetMenu(fileName = "UpgradeData", menuName = "ScriptableObjects/UpgradeData")]
public class UpgradeData : ScriptableObject
{
	public enum UpgradeType
	{
		Attack, Defense, Special, Weapon
	}

	public Weapon weapon;
	public new string name;
	public string description;
	public UpgradeType upgradeType;
	public Sprite icon;
	public float healthMultiplier = 1;
	public float damageMultiplier = 1;
	public float attractorRangeMultiplier = 1;
	public Color color;
}