﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/PlayerData")]
public class PlayerData : ScriptableObject
{
    public Weapon[] weapons;
    public int level;
    public float health;
    public float healthMax;
    public float damageMultiplier;
    public float xp;
    public float xpMax;
    public float xpMaxMultiplier;
    public float attractorRange;
    public bool godMode;

    public event Action onValueChanged;
    public void ValueChanged() => onValueChanged?.Invoke();
}
