using TMPro;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "ScriptableObjects/GameData", order = 1)]
public class GameData : ScriptableObject
{
	public PlayerData playerData;
	public TMP_FontAsset defaultFont;
	public Sprite colorPalette;
	public ColorData[] colors;
}