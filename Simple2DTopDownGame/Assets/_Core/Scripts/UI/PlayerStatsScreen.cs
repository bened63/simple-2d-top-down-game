﻿using System;
using TMPro;
using UnityEngine;

public class PlayerStatsScreen : BeGoodUI
{
	[SerializeField] private PlayerData _playerData;
	[SerializeField] private PlayerStatsItem _level;
	[SerializeField] private PlayerStatsItem _health;
	[SerializeField] private PlayerStatsItem _damage;
	[SerializeField] private PlayerStatsItem _attractorRange;

	private void Awake()
	{
		if (_playerData == null)
		{
			Debug.LogError("PlayerData not referenced in PlayerStatsScreen.");
		}
	}

	private void Start()
	{
		UpdateStats();
	}

	public void UpdateStats()
	{
		_level.value = _playerData.level.ToString();
		_health.value = Mathf.RoundToInt(_playerData.healthMax).ToString();
		_damage.value = $"{Mathf.RoundToInt(_playerData.damageMultiplier * 100f)} %";
		_attractorRange.value = $"{Mathf.RoundToInt(_playerData.attractorRange * 100f)} %";
	}
}