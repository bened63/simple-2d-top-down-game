﻿using UnityEngine;

public class LevelUpScreen : BeGoodUI
{
	[Header("References")]
	[SerializeField] private UpgradePanel[] _upgradePanels;

	private PlayerData _playerData;

	private void Start()
	{
		if (GameManager.instance.playerData != null)
		{
			_playerData = GameManager.instance.playerData;
		}
		else
		{
			Debug.LogError("[LevelUpPopup] PlayerData missing in GameManager > GameData");
		}
	}

	private void Update()
	{
	}

	public void ClickOnUpgradePanel(int index)
	{
		// Get UpgradeData
		index = Mathf.Clamp(index, 0, _upgradePanels.Length - 1);
		UpgradeData upgradeData = _upgradePanels[index].upgradeData;

		// Upgrade Player data
		UpgradePlayerData(upgradeData);
	}

	private void UpgradePlayerData(UpgradeData upgradeData)
	{
		// Level
		_playerData.level++;

		// Damage
		_playerData.damageMultiplier *= upgradeData.damageMultiplier;

		// Health
		_playerData.health *= upgradeData.healthMultiplier;
		_playerData.healthMax *= upgradeData.healthMultiplier;

		// XP
		_playerData.xp = 0;
		_playerData.xpMax *= _playerData.xpMaxMultiplier;

		// Attractor
		_playerData.attractorRange *= upgradeData.attractorRangeMultiplier;

		// ==== INFORM PLAYER ====
		_playerData.ValueChanged();

		// Close popup
		GameManager.instance.CloseLevelUpPopup();
	}
}