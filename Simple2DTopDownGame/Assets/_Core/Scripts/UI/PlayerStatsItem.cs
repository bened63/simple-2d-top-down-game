using TMPro;
using UnityEngine;

public class PlayerStatsItem : MonoBehaviour
{
	[SerializeField] private bool _useGameObjectNameAsLabel;
	[SerializeField] private TextMeshProUGUI _label;
	[SerializeField] private TextMeshProUGUI _value;

	public new string name
	{
		get { return _label.name; }
	}


	public string value
	{
		get { return _value.text; }
		set { _value.text = value; }
	}


	// Start is called before the first frame update
	private void Start()
	{
	}

	// Update is called once per frame
	private void Update()
	{
	}

	private void OnValidate()
	{
		if (_useGameObjectNameAsLabel)
		{
			_label.text = gameObject.name;
		}
	}
}