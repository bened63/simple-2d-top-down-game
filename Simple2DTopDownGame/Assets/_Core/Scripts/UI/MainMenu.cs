using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MainMenu : BeGoodUI
{
	[SerializeField] private AudioClip _hoverSound;
	[SerializeField] private AudioClip _clickSound;
	private AudioSource _audioSource;

	private void Awake()
	{
		_audioSource = GetComponent<AudioSource>();
	}

	public void PlayHoverSound() => _audioSource.PlayOneShot(_hoverSound);

	public void PlayClickSound() => _audioSource.PlayOneShot(_clickSound);
}