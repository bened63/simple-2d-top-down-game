﻿using System.Collections;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class BeGoodUI : MonoBehaviour
{
	//[SerializeField] private bool _useForAllChildren = true;
	//[SerializeField] private TMP_FontAsset _font;
	//[SerializeField] private Color _color;

	private CanvasGroup _canvasGroup;
	private Coroutine _fadeCoroutine;

	public bool isActive
	{
		get { return gameObject.activeInHierarchy; }
		private set { gameObject.SetActive(value); }
	}


	private void Start()
	{
		Init();
	}

	private void OnEnable()
	{
		Init();
	}

	private void OnValidate()
	{
		//if (!Application.isPlaying && _useForAllChildren)
		//{
		//	TextMeshProUGUI[] tmps = transform.GetComponentsInChildren<TextMeshProUGUI>();
		//	foreach (TextMeshProUGUI item in tmps)
		//	{
		//		item.font = _font ?? TMP_Settings.defaultFontAsset;
		//		item.color = _color;
		//	}
		//}
	}

	private void Init()
	{
		_canvasGroup = GetComponent<CanvasGroup>();
	}

	public void Show()
	{
		if (!isActive) isActive = true;
		_canvasGroup.alpha = 1f;
	}

	public void Hide()
	{
		if (!isActive) isActive = true;
		_canvasGroup.alpha = 0f;
		if (isActive) isActive = false;
	}

	public void FadeTo(float alpha, float duration)
	{
		if (_fadeCoroutine != null) StopCoroutine(_fadeCoroutine);
		_fadeCoroutine = StartCoroutine(IEFade(alpha , duration));
	}

	private IEnumerator IEFade(float alpha, float duration)
	{
		float currentLerpTime = 0f;
		float lerpTime = duration;
		bool run = true;
		float start = _canvasGroup.alpha;
		float end = alpha;

		while (run)
		{
			//increment timer once per frame
			currentLerpTime = Mathf.Clamp(currentLerpTime += Time.deltaTime, 0f, lerpTime);
			if (lerpTime <= 0) lerpTime = currentLerpTime;

			//lerp!
			float t = currentLerpTime / lerpTime;
			_canvasGroup.alpha = Mathf.Lerp(start, end, t);

			//finish
			run = currentLerpTime < lerpTime;

			yield return null;
		}
	}

}