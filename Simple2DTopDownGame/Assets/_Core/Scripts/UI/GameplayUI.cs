﻿using TMPro;
using UnityEngine;

public class GameplayUI : BeGoodUI
{
	[Header("References")]
	[SerializeField] private Healthbar _healthbar;
	[SerializeField] private Healthbar _xpbar;
	[SerializeField] private TextMeshProUGUI _levelText;

	public Healthbar healthbar { get => _healthbar; private set => _healthbar = value; }
	public Healthbar xpbar { get => _xpbar; private set => _xpbar = value; }
	public int level { set => _levelText.text = value.ToString(); }

	private void Start()
	{
	}

	private void Update()
	{
	}
}
