using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePanel : MonoBehaviour
{
	[SerializeField] private UpgradeData _upgradeData;
	[SerializeField] private TextMeshProUGUI _name;
	[SerializeField] private TextMeshProUGUI _description;
	[SerializeField] private Image _iconImage;

	public UpgradeData upgradeData
	{
		get => _upgradeData;
		set
		{
			_upgradeData = value;
			UpdatePanel();
		}
	}

	// Start is called before the first frame update
	private void Start()
	{
	}

	// Update is called once per frame
	private void Update()
	{
	}

	private void OnValidate()
	{
		UpdatePanel();
	}

	private void UpdatePanel()
	{
		if (upgradeData == null)
		{
			return;
		}

		if (_name != null) _name.text = upgradeData.name;
		if (_description != null) _description.text = upgradeData.description;
		if (_iconImage != null) _iconImage.sprite = upgradeData.icon;
		gameObject.GetComponent<Image>().color = upgradeData.color;
	}
}