﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class DebugUI : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private TextMeshProUGUI _fps;
	[SerializeField] private TextMeshProUGUI _enemyCount;
	[SerializeField] private TextMeshProUGUI _collectibleCount;

	private CanvasGroup _canvasGroup;
	private float _fpsTime = 0;
	private float _fpsUpdateTime = 0.25f;
	private int _lastFrameIndex = 0;
	private float[] _frameDeltaTimeArray = new float[50];

	private void Start()
	{
		_canvasGroup = GetComponent<CanvasGroup>();
	}

	private void Update()
	{
		CalculateFPS();
	}

	private void CalculateFPS()
	{
		_frameDeltaTimeArray[_lastFrameIndex] = Time.deltaTime;
		_lastFrameIndex = (_lastFrameIndex + 1) % _frameDeltaTimeArray.Length;

		float sum = 0f;
		foreach (float delta in _frameDeltaTimeArray)
		{
			sum += delta;
		}
		int avgFPS = Mathf.RoundToInt(_frameDeltaTimeArray.Length / sum);

		_fpsTime += Time.unscaledDeltaTime;
		if (_fpsTime >= _fpsUpdateTime)
		{
			_fpsTime = 0;
			_fps.text = $"FPS: {avgFPS}";
		}
	}

	public void Toggle() => Toggle(_canvasGroup.alpha == 0);

	public void Toggle(bool show)
	{
		if (show) Show();
		else Hide();
	}

	public void Show() => _canvasGroup.alpha = 1f;

	public void Hide() => _canvasGroup.alpha = 0f;

	public void SetEnemyCount(int count) => _enemyCount.text = $"Enemy Count: {count}";

	public void SetCollectibleCount(int count) => _collectibleCount.text = $"Collectible Count: {count}";
}