using System.Collections;
using UnityEngine;

public class Spawner2D : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] private bool _spawnAtStart;
	[SerializeField] private string _prefabName;
	[SerializeField] private GameObject _entityToSpawn;
	[SerializeField] public bool _randomRotation;
	[SerializeField, Min(0)] public int _numberOfPrefabsToSpawn;
	[SerializeField, Min(0)] private float _spawnRadius;
	[SerializeField, Range(0f, 1f)] private float _delayBetweenSpawnsMultiplier;

	[Header("Respawn Settings")]
	[SerializeField] private bool _respawn = false;
	[SerializeField] private float _respawnTime = 5;

	private bool _gamePaused = false;

	public int spawnCount { get; private set; }

	// Start is called before the first frame update
	private void Start()
	{
		GameManager.instance.onGamePaused += (b) => _gamePaused = b;
		if (_spawnAtStart) SpawnEntities();
	}

	private void SpawnEntities()
	{
		StartCoroutine(IESpawnEntities());
	}

	private IEnumerator IESpawnEntities(float delayBeforeStart = 0f)
	{
		yield return new WaitForSeconds(delayBeforeStart);

		bool run = true;

		// This will be appended to the name of the created entities and increment when each is created
		int instanceNumber = 1;

		while (run)
		{
			for (int i = 0; i < _numberOfPrefabsToSpawn; i++)
			{
				yield return new WaitWhile(() => _gamePaused);

				float delay = Random.Range(0.01f, 0.5f) * _delayBetweenSpawnsMultiplier;
				yield return new WaitForSeconds(delay);

				// Position inside a sphere with radius {_spawnRadius} and the center at zero
				Vector3 pos = Random.insideUnitCircle * _spawnRadius;

				// Add offset
				pos += transform.position;

				// Rotation
				Quaternion rotation = Quaternion.identity;
				if (_randomRotation)
				{
					rotation = Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f));
				}

				// Create an instance of the prefab
				GameObject go = Instantiate(_entityToSpawn, pos, rotation) as GameObject;

				// Set the name of the instantiated entity
				go.name = string.Format("{0}_{1}", _prefabName, instanceNumber.ToString("00"));

				// Set parent
				go.transform.parent = transform;

				// Increase instance number
				instanceNumber++;
			}

			// Save count of spawned entities
			spawnCount = instanceNumber;

			run = _respawn;

			yield return new WaitWhile(() => _gamePaused);
			yield return new WaitForSeconds(_respawnTime);
			yield return new WaitWhile(() => _gamePaused);
		}
	}

	private void OnDrawGizmos()
	{
#if UNITY_EDITOR
		UnityEditor.Handles.color = Color.red;
		UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, _spawnRadius);
#endif
	}
}