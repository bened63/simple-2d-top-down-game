using UnityEngine;

public class KrangAI : MonoBehaviour
{
	public enum Difficulty
	{
		Easy, Medium
	}

	#region Singleton

	public static KrangAI instance;

	private void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	#endregion Singleton

	[SerializeField] private Difficulty _difficulty;
	[SerializeField] private GameObject[] _enemySpawnerPrefabs;
	[SerializeField] private Spawner2D[] _enemySpawners;

	private Player _player;

	// Start is called before the first frame update
	private void Start()
	{
		_player = FindObjectOfType<Player>();
	}

	// Update is called once per frame
	private void Update()
	{
	}
}