using System;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
	[SerializeField] private AudioMixerGroup _mixerGroup;
	[SerializeField] private Sound[] _sounds;

	private float _volumeMultiplier = 30f;

	#region Singleton

	public static AudioManager instance;

	private void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}

		foreach (Sound s in _sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.loop = s.loop;

			s.source.outputAudioMixerGroup = _mixerGroup;
		}
	}

	#endregion Singleton

	private void Start()
	{
		foreach (Sound s in _sounds)
		{
			if (s.playOnAwake)
			{
				Play(s.name);
			}
		}
	}

	public void Play(string sound)
	{
		Sound s = Array.Find(_sounds, item => item.name == sound);
		if (s == null)
		{
			Debug.LogWarning("Sound: " + name + " not found!");
			return;
		}

		s.source.volume = s.volume * (1f + UnityEngine.Random.Range(-s.volumeVariance / 2f, s.volumeVariance / 2f));
		s.source.pitch = s.pitch * (1f + UnityEngine.Random.Range(-s.pitchVariance / 2f, s.pitchVariance / 2f));

		s.source.Play();
	}

	public void SetVolume(float volume)
	{
		float value = Mathf.Clamp(volume, 0.0001f, 1f);
		_mixerGroup.audioMixer.SetFloat("MasterVolume", Mathf.Log10(value) * _volumeMultiplier);
	}
}