using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public enum LevelName
{
	MainMenu, Level_1, Level_2
}

public class LevelManager : MonoBehaviour
{
	[SerializeField] private LevelName[] _levels;

	private int _currentLevelIndex = -1;

	#region Singleton

	public static LevelManager instance;

	private void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	#endregion Singleton

	private void Start()
	{
		for (int i = 0; i < SceneManager.sceneCount; i++)
		{
			Debug.LogFormat("<color=lime>Scene: {0}</color>", SceneManager.GetSceneAt(i).name);
			if (SceneManager.GetSceneAt(i).name != "MainMenu")
			{
				SceneManager.UnloadScene(SceneManager.GetSceneAt(i).name);
			}
		}
	}

	private IEnumerator LoadLevelAsync(LevelName name)
	{
		// The Application loads the Scene in the background as the current Scene runs.
		// This is particularly good for creating loading screens.
		// You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
		// a sceneBuildIndex of 1 as shown in Build Settings.
		
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name.ToString());

		// Wait until the asynchronous scene fully loads
		while (!asyncLoad.isDone)
		{
			yield return null;
		}
	}

	public void NextLevel()
	{
		_currentLevelIndex++;

		if (_currentLevelIndex >= _levels.Length)
		{
			_currentLevelIndex = 0;
		}
		else if (_currentLevelIndex < 0)
		{
			_currentLevelIndex = _levels.Length - 1;
		}

		StartCoroutine(LoadLevelAsync(_levels[_currentLevelIndex]));
	}

	public void LoadLevel(LevelName name)
	{
		_currentLevelIndex = (int)name;
		StartCoroutine(LoadLevelAsync(name));
	}
}