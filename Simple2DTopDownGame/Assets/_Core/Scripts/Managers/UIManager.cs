using System;
using System.Collections;
using UnityEngine;

public class UIManager : MonoBehaviour
{
	[SerializeField] private MainMenu _mainMenu;
	[SerializeField] private GameplayUI _gameplayUI;
	[SerializeField] private DebugUI _debugUI;
	[SerializeField] private LevelUpScreen _levelUpScreen;
	[SerializeField] private PlayerStatsScreen _playerStatsScreen;
	[SerializeField] private GameOverScreen _gameOverScreen;
	[SerializeField] private BeGoodUI _blackScreen;
	[SerializeField] private bool _showDebugInfos;

	private GameManager _gameManager;
	private Canvas _canvas;
	private CanvasGroup _canvasGroup;
	private Coroutine _lerpAlphaCoroutine;

	#region Singleton

	public static UIManager instance;

	private void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		AwakeInit();
	}

	#endregion Singleton

	public GameplayUI gameplayUI { get => _gameplayUI; set => _gameplayUI = value; }
	public DebugUI debugUI { get => _debugUI; set => _debugUI = value; }

	private bool _levelScreenActive;

	public bool levelScreenActive
	{
		get => _levelScreenActive;
		set
		{
			if (value != _levelScreenActive)
			{
				_gameManager.isPaused = value;
				_levelScreenActive = value;
				if (_levelScreenActive) _levelUpScreen.Show();
				else _levelUpScreen.Hide();
			}
		}
	}

	public bool gameOverScreenActive
	{
		get => _gameOverScreen.isActive;
		set
		{
			if (value != _gameOverScreen.isActive)
			{
				_gameManager.isPaused = value;
				if (value) _gameOverScreen.Show();
				else _gameOverScreen.Hide();
			}
		}
	}

	public bool playerStatsScreenActive
	{
		get => _playerStatsScreen.isActive;
		set
		{
			_gameManager.isPaused = value;
			_playerStatsScreen.UpdateStats();
			if (value) _playerStatsScreen.Show();
			else _playerStatsScreen.Hide();
		}
	}

	private void AwakeInit()
	{
		_canvas = transform.GetComponentInChildren<Canvas>();
		_canvasGroup = _canvas.gameObject.AddComponent<CanvasGroup>();

		if (!_mainMenu)
		{
			Debug.LogError("No MainMenu referenced");
		}

		if (!_gameplayUI)
		{
			Debug.LogError("No GameplayUI referenced");
		}
	}

	public void FadeBlackScreenTo(float alpha, float duration)
	{
		_blackScreen.FadeTo(alpha, duration);
	}

	private void Start()
	{
		_gameManager = GameManager.instance;
	}

	public void ShowUI()
	{
		if (_lerpAlphaCoroutine != null) StopCoroutine(_lerpAlphaCoroutine);
		_lerpAlphaCoroutine = StartCoroutine(IELerpAlpha(true));
	}

	public void HideUI()
	{
		if (_lerpAlphaCoroutine != null) StopCoroutine(_lerpAlphaCoroutine);
		_lerpAlphaCoroutine = StartCoroutine(IELerpAlpha(false));
	}

	private IEnumerator IELerpAlpha(bool show)
	{
		float currentLerpTime = 0f;
		float lerpTime = 0.2f;
		bool run = true;

		float start = _canvasGroup.alpha;
		float end = show ? 1f : 0f;

		while (run)
		{
			//increment timer once per frame
			currentLerpTime = Mathf.Clamp(currentLerpTime + Time.deltaTime, 0f, lerpTime);

			//lerp!
			float t = currentLerpTime / lerpTime;
			t = t * t * t * (t * (6f * t - 15f) + 10f);
			_canvasGroup.alpha = Mathf.Lerp(start, end, t);

			//end check
			run = currentLerpTime < lerpTime;

			yield return null;
		}
	}

	public void ShowMainMenu()
	{
		_debugUI.Toggle(_showDebugInfos);

		_gameplayUI.Hide();
		_levelUpScreen.Hide();
		_gameOverScreen.Hide();
		_playerStatsScreen.Hide();

		_mainMenu.Show();
	}

	public void HideMainMenu()
	{
		_mainMenu.Hide();
	}

	public void ShowGameplayUI()
	{
		_gameplayUI.Show();
	}

	public void HideGameplayUI()
	{
		_gameplayUI.Hide();
	}
}