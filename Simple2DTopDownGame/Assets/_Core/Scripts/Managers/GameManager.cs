using System;
using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	[SerializeField] private bool _startLevelOnAwake;
	[SerializeField] private GameData _gameData;

	private Coroutine _startGameCoroutine;

	private int _enemyCount;

	public int enemyCount
	{
		get => _enemyCount;
		set
		{
			_enemyCount = value;
			UIManager.instance.debugUI.SetEnemyCount(_enemyCount);
		}
	}

	private int _collectibleCount;

	public int collectibleCount
	{
		get => _collectibleCount;
		set
		{
			_collectibleCount = value;
			UIManager.instance.debugUI.SetCollectibleCount(_collectibleCount);
		}
	}

	private bool _isPaused;

	public bool isPaused
	{
		get => _isPaused;
		set
		{
			if (value != _isPaused)
			{
				_isPaused = value;
				onGamePaused?.Invoke(_isPaused);
				if (_isPaused) Debug.LogFormat("<color=lime>===============PAUSED===============</color>");
				if (!_isPaused) Debug.LogFormat("<color=lime>===============PLAY===============</color>");
			}
		}
	}

	public PlayerData playerData
	{
		get => _gameData.playerData;
	}

	public GameData gameData
	{
		get => _gameData;
	}

	public event Action<bool> onGamePaused;

	public event Action onGameStarted;

	public event Action onPlayerDataChanged;

	#region Singleton

	public static GameManager instance;
	private UIManager _uiManager;

	private void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	#endregion Singleton

	private void Start()
	{
		_uiManager = UIManager.instance;
		_uiManager.ShowMainMenu();

		if (_startLevelOnAwake)
		{
			StartGame();
		}
	}

	private void Update()
	{
	}

	private IEnumerator IEStartGameCoroutine()
	{
		float fadeDuration = 0.3f;

		_uiManager.FadeBlackScreenTo(1, fadeDuration);
		yield return new WaitForSeconds(fadeDuration);

		LevelManager.instance.NextLevel();
		UIManager.instance.HideMainMenu();
		UIManager.instance.ShowGameplayUI();
		UIManager.instance.gameOverScreenActive = false;

		_uiManager.FadeBlackScreenTo(0, fadeDuration);
		yield return new WaitForSeconds(fadeDuration);

		AudioManager.instance.Play("Ambient_Level_1");
		isPaused = false;
		onGameStarted?.Invoke();

		yield return null;
	}

	public void StartGame()
	{
		if (_startGameCoroutine != null) StopCoroutine(_startGameCoroutine);
		_startGameCoroutine = StartCoroutine(IEStartGameCoroutine());
	}

	public void LevelUp()
	{
		isPaused = true;
		_uiManager.levelScreenActive = true;
	}

	public void CloseLevelUpPopup()
	{
		isPaused = false;
		_uiManager.levelScreenActive = false;
	}

	public void GameOver()
	{
		isPaused = true;
		_uiManager.gameOverScreenActive = true;
		StartCoroutine(IEShowMainMenuInSeconds(5));
	}

	private IEnumerator IEShowMainMenuInSeconds(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		_uiManager.ShowMainMenu();
	}
}
