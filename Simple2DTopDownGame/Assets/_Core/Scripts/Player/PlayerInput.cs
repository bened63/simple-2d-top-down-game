using System;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
	public float horizontalInput { get; set; }
	public float verticalInput { get; set; }

	private KeyCode _keyPressed;

	public KeyCode keyPressed
	{
		get => _keyPressed;
		set
		{
			_keyPressed = value;
			onKeyPressed?.Invoke(_keyPressed);
		}
	}

	public event Action<KeyCode> onKeyPressed;

	// Start is called before the first frame update
	private void Start()
	{
	}

	// Update is called once per frame
	private void Update()
	{
		horizontalInput = Input.GetAxisRaw("Horizontal");
		verticalInput = Input.GetAxisRaw("Vertical");

		if (Input.GetKeyDown(KeyCode.U))
		{
			keyPressed = KeyCode.U;
		}

		if (Input.GetKeyDown(KeyCode.P))
		{
			keyPressed = KeyCode.P;
		}

		if (Input.GetKeyDown(KeyCode.I))
		{
			keyPressed = KeyCode.I;
		}
	}
}