using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
public class PlayerMovement : MonoBehaviour
{
	[SerializeField] private float _moveSpeed = 5;

	private Rigidbody2D _rb;
	private Vector3 _moveDirection;
	private PlayerInput _input;

	public bool canMove { get; set; }

	// Start is called before the first frame update
	private void Start()
	{
		_input = GetComponent<PlayerInput>();
		_rb = GetComponent<Rigidbody2D>();
		canMove = true;
	}

	// Update is called once per frame
	private void Update()
	{
		if (!canMove)
		{
			return;
		}

		CalculateMoveDirection();
		Move();
	}

	private void CalculateMoveDirection()
	{
		_moveDirection = new Vector3(_input.horizontalInput, _input.verticalInput, 0);
		_moveDirection = Vector3.Normalize(_moveDirection);
	}

	private void Move()
	{
		//if (_dash)
		//{
		//	_dashTimer = 0;
		//	_canDash = false;
		//	_dash = false;
		//	StartCoroutine(IEDash());
		//}
		//transform.Translate(_movement * _moveSpeed * Time.deltaTime);
		//_rb.velocity = _moveDirection * _moveSpeed * Time.deltaTime;

		transform.Translate(_moveDirection * Time.deltaTime * _moveSpeed);
	}
}