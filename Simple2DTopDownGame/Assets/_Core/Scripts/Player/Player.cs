using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private PlayerData _playerData;
	[SerializeField] private WeaponHolder _weaponHolder;
	[SerializeField] private Attractor _attractor;

	[Header("Settings")]
	[SerializeField] private int _startLevel = 1;
	[SerializeField] private float _startHealth = 150;
	[SerializeField] private float _startHealthMax = 150;
	[SerializeField] private float _startXP = 0;
	[SerializeField] private float _startXPMax = 10;
	[SerializeField] private float _startXPMaxMultiplier = 1.25f;
	[SerializeField] private float _startDamageMultiplier = 1;
	[SerializeField] private float _startAttractorRange = 1;

	private int _level;
	private float _health, _healthMax;
	private float _xp, _xpMax;
	private float _attractorRange;

	private GameManager _gameManager;
	private UIManager _uiManager;
	private bool _godMode = false;
	private PlayerInput _input;
	private PlayerMovement _movement;

	public int level
	{
		get => _level;
		set
		{
			_level = value;
			_playerData.level = _level;
			_uiManager.gameplayUI.level = _level;
		}
	}

	public float health
	{
		get => _health;
		set
		{
			_health = value;
			_playerData.health = _health;
			_uiManager.gameplayUI.healthbar.value = _health;
		}
	}

	public float healthMax
	{
		get => _healthMax;
		set
		{
			_healthMax = value;
			_playerData.healthMax = _healthMax;
			_uiManager.gameplayUI.healthbar.valueMax = _healthMax;
		}
	}

	public float xp
	{
		get => _xp;
		set
		{
			_xp = value;
			_playerData.xp = _xp;
			_uiManager.gameplayUI.xpbar.value = _xp;

			if (_xp >= _playerData.xpMax)
			{
				_gameManager.LevelUp();
			}
		}
	}

	public float xpMax
	{
		get => _xpMax;
		set
		{
			_xpMax = value;
			_playerData.xpMax = _xpMax;
			_uiManager.gameplayUI.xpbar.valueMax = _xpMax;
		}
	}

	public float attractorRange
	{
		get => _attractorRange;
		set
		{
			_attractorRange = value;
			_playerData.attractorRange = _attractorRange;
			_attractor.radius = _attractorRange;
		}
	}

	private void Start()
	{
		_movement = GetComponent<PlayerMovement>();

		_input = GetComponent<PlayerInput>();
		_input.onKeyPressed += HandleOnKeyPressed;

		_uiManager = UIManager.instance;

		_gameManager = GameManager.instance;
		_gameManager.onGamePaused += HandleOnGamePaused;

		_playerData.onValueChanged += HandleOnPlayerDataChanged;

		InitPlayerData();
	}

	private void InitPlayerData()
	{
		level = _startLevel;
		healthMax = _startHealthMax;
		health = _startHealth;
		xpMax = _startXPMax;
		xp = _startXP;
		attractorRange = _startAttractorRange;

		_playerData.xpMaxMultiplier = _startXPMaxMultiplier;
		_playerData.damageMultiplier = _startDamageMultiplier;
	}

	private void HandleOnKeyPressed(KeyCode key)
	{
		if (key == KeyCode.U)
		{
			_uiManager.levelScreenActive = !_uiManager.levelScreenActive;
		}

		if (key == KeyCode.P)
		{
			_gameManager.isPaused = !_gameManager.isPaused;
		}

		if (key == KeyCode.I)
		{
			_uiManager.playerStatsScreenActive = !_uiManager.playerStatsScreenActive;
		}
	}

	private void HandleOnGamePaused(bool pause)
	{
		_movement.canMove = !pause;
	}

	private void HandleOnPlayerDataChanged() => UpdatePlayerData();

	private void UpdatePlayerData()
	{
		level = _playerData.level;
		attractorRange = _playerData.attractorRange;
		xp = _playerData.xp;
		health = _playerData.health;
	}

	public void Hit(float damage)
	{
		if (_gameManager.isPaused)
			return;

		health -= damage;
		if (health <= 0f)
		{
			Die();
		}
	}

	private void Die()
	{
		_gameManager.GameOver();
	}

	private IEnumerator IEGodModeForSeconds(float seconds)
	{
		_godMode = true;
		yield return new WaitForSeconds(seconds);
		_godMode = false;
	}

	public void IncreaseXP(int amount)
	{
		xp += amount;
	}
}