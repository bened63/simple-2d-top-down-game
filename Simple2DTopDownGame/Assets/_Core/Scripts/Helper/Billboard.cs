using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class Billboard : MonoBehaviour
{
	[SerializeField] private string _cameraTag = "MainCamera";
	private Camera _cam;

	private void Awake()
	{
		GameObject camGo = GameObject.FindWithTag(_cameraTag);
		if (camGo)
		{
			_cam = GetComponent<Camera>();
			GetComponent<Canvas>().worldCamera = _cam;
		}
		else
		{
			Debug.LogError($"No camera with tag '{_cameraTag}' found!");
		}
	}

	private void LateUpdate()
	{
		if (_cam)
		{
			transform.LookAt(transform.position + _cam.transform.forward);
		}
	}
}