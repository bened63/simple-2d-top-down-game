using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public static class Toolbox
{
	public static class Easings
	{
		// See: https://easings.net/de

		public enum Function
		{
			Linear,
			Smoothstep,
			Smootherstep,
			EaseInOutBack,
			EaseOutElastic,
			EaseInOutElastic
		}

		public static float Interpolate(float t, Function function = Function.Linear)
		{
			switch (function)
			{
				case Function.Linear:
					return t;

				case Function.Smoothstep:
					return t * t * (3f - 2f * t);

				case Function.Smootherstep:
					return t * t * t * (t * (6f * t - 15f) + 10f);

				case Function.EaseInOutBack:
					float c1 = 1.70158f;
					float c2 = c1 * 1.525f;

					return t < 0.5
					  ? (Mathf.Pow(2 * t, 2f) * ((c2 + 1f) * 2f * t - c2)) / 2f
					  : (Mathf.Pow(2 * t - 2f, 2f) * ((c2 + 1f) * (t * 2f - 2f) + c2) + 2f) / 2f;

				case Function.EaseOutElastic:
					float c4 = (2f * Mathf.PI) / 3f;

					return t == 0
					  ? 0
					  : t == 1
					  ? 1
					  : Mathf.Pow(2f, -10f * t) * Mathf.Sin((t * 10f - 0.75f) * c4) + 1f;

				case Function.EaseInOutElastic:
					float c5 = (2f * Mathf.PI) / 4.5f;

					return t == 0
					  ? 0
					  : t == 1
					  ? 1
					  : t < 0.5f
					  ? -(Mathf.Pow(2f, 20f * t - 10f) * Mathf.Sin((20f * t - 11.125f) * c5)) * 0.5f
					  : (Mathf.Pow(2f, -20f * t + 10f) * Mathf.Sin((20f * t - 11.125f) * c5)) * 0.5f + 1;

				default:
					return t;
			}
		}
	}
	public static TextMeshProUGUI CreateTextMeshPro(GameObject goParent, string name, float width = 200, float height = 100, TMP_FontAsset font = null, int fontSize = 30, Color? color = null)
	{
		GameObject goLabel = new GameObject(name);
		goLabel.transform.SetParent(goParent.transform);
		goLabel.transform.localPosition = new Vector3(0, 0, 0);
		goLabel.transform.localScale = new Vector3(1, 1, 1);

		TextMeshProUGUI txt = goLabel.AddComponent<TextMeshProUGUI>();
		txt.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
		txt.alignment = TextAlignmentOptions.Center;
		txt.text = "My text";
		txt.color = color ?? Color.white;
		txt.fontSize = fontSize;
		txt.font = font ?? TMP_Settings.defaultFontAsset;

		return txt;
	}

	public static Image CreateImage(GameObject goParent, string name, string spritepath, float width = -1, float height = -1, Color? color = null, Image.Type imageType = Image.Type.Simple, Image.FillMethod fillMethod = Image.FillMethod.Horizontal)
	{
		GameObject go = new GameObject(name);
		go.transform.SetParent(goParent.transform);
		go.transform.localPosition = new Vector3(0, 0);
		go.transform.localScale = new Vector3(1, 1, 1);

		Image img = go.AddComponent<Image>();
		img.sprite = Resources.Load<Sprite>(spritepath);
		img.SetNativeSize();

		RectTransform rt = go.GetComponent<RectTransform>();
		if (width != -1) rt.sizeDelta = new Vector2(width, rt.sizeDelta.y);
		if (height != -1) rt.sizeDelta = new Vector2(rt.sizeDelta.x, height);

		img.color = color ?? Color.white;
		img.type = imageType;
		img.fillMethod = fillMethod;
		img.fillOrigin = 0;

		return img;
	}

	public static RawImage CreateRawImage(GameObject parent, string name, Texture texture, int width, int height, Color? color = null)
	{
		GameObject go = new GameObject(name);
		go.transform.SetParent(parent.transform);
		go.transform.localPosition = Vector3.zero;
		go.transform.localScale = Vector3.one;
		go.transform.localRotation = Quaternion.identity;

		RawImage rawImg = go.AddComponent<RawImage>();
		rawImg.texture = texture;
		rawImg.SetNativeSize();

		RectTransform rt = go.GetComponent<RectTransform>();
		rt.sizeDelta = new Vector2(width, height);

		rawImg.color = color ?? Color.white;
		return rawImg;
	}

	/// <summary>
	/// Maps float value from one range to another range. Result get clamped.
	/// </summary>
	public static float ValueMapper(float value, float valMin, float valMax, float outMin, float outMax)
	{
		float v = Mathf.Clamp01((value - valMin) / (valMax - valMin));
		return v * (outMax - outMin) + outMin;
	}

	/// <summary>
	/// Maps float value from one range to another range. Result is clamped, rounded and converted to an integer.
	/// </summary>
	public static int ValueMapperToInt(float value, float valMin, float valMax, int outMin, int outMax)
	{
		return Mathf.RoundToInt(ValueMapper(value, valMin, valMax, outMin, outMax));
	}

	/// <summary>
	/// Convert an byte array into an integer bit array (contains only 0s and 1s).
	/// </summary>
	public static List<int> ByteArrayToIntList(byte[] b, bool reverse = false, bool reverseComplete = false)
	{
		List<int> list = new List<int>();
		BitArray t;
		bool[] bits = new bool[8];

		for (int x = 0; x < b.Length; x++)
		{
			t = new BitArray(new byte[] { b[x] }); //you can convert more than one byte, but for simplicity I'm doing one at a time
			t.CopyTo(bits, 0);
			if (reverse) Array.Reverse(bits);
			foreach (bool bit in bits) list.Add(bit ? 1 : 0);
		}

		if (reverseComplete) list.Reverse();

		return list;
	}

	/// <summary>
	/// Convert an byte array to a string of bits (0s and 1s).
	/// </summary>
	public static string ConvertLittleEndian(byte[] a)
	{
		string[] b = a.Select(x => Convert.ToString(x, 2).PadLeft(8, '0')).ToArray();
		string s = string.Join(" ", a.Select(x => Convert.ToString(x, 2).PadLeft(8, '0')));

		return s;
	}

	/// <summary>
	/// Calculate the angle between two Vector2 vectors.
	/// </summary>
	private static float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
	{
		Vector2 vec1Rotated90 = new Vector2(-vec1.y, vec1.x);
		float sign = (Vector2.Dot(vec1Rotated90, vec2) < 0) ? -1.0f : 1.0f;
		return Vector2.Angle(vec1, vec2) * sign;
	}

	/// <summary>
	/// Calculate the angle between two Vector2 vectors.
	/// </summary>
	public static void LookAt2D(Transform from, Transform to)
	{
		Vector3 dir = to.position - from.position;
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		from.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}
}