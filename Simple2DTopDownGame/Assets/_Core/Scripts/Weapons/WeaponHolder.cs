using System.Collections.Generic;
using UnityEngine;

public class WeaponHolder : MonoBehaviour
{
	[SerializeField] private string _startWeaponName;
	[SerializeField] private Transform _weaponPosition;
	[SerializeField] private List<Weapon> _equippedWeapons;
	[SerializeField] private Weapon[] _availableWeapons;

	// Start is called before the first frame update
	private void Start()
	{
		_equippedWeapons = new List<Weapon>();
		EquipWeapon(_startWeaponName);
	}

	// Update is called once per frame
	private void Update()
	{
	}

	public void EquipWeapon(string weaponName)
	{
		foreach (Weapon w in _availableWeapons)
		{
			if (weaponName == w.name)
			{
				_equippedWeapons.Add(Instantiate(w, _weaponPosition));
			}
		}
	}
}