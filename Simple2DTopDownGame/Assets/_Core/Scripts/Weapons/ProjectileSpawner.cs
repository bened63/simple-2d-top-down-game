using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ProjectileSpawner : MonoBehaviour
{
	private ParticleSystem _part;
	private List<ParticleCollisionEvent> _collisionEvents;

	public event Action<GameObject> onCollision;

	private void Start()
	{
		_part = GetComponent<ParticleSystem>();
		_collisionEvents = new List<ParticleCollisionEvent>();
	}

	private void OnParticleCollision(GameObject other)
	{
		int numCollisionEvents = _part.GetCollisionEvents(other, _collisionEvents);

		int i = 0;

		while (i < numCollisionEvents)
		{
			onCollision?.Invoke(other);
			i++;
		}
	}

	public void Emit()
	{
		if (_part.emission.burstCount > 0)
		{
			float c = _part.emission.GetBurst(0).count.constant;
			_part.Emit(Mathf.RoundToInt(c));
		}
		else
		{
			_part.Emit(1);
		}
	}
}