using Cinemachine;
using System;
using UnityEngine;

public class Weapon : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private ProjectileSpawner _projectileSpawner;
	[SerializeField] private AudioClip _shootSound;

	[Header("Settings")]
	[SerializeField] private new string name;
	[SerializeField, Min(1f)] private float _damagePerProjectile = 1;
	[SerializeField, Min(0.01f)] private float _fireRate = 1f;
	[SerializeField] private bool _autoFire = true;
	[SerializeField] private float _shakeStrength = 0.1f;

	private AudioSource _audioSource;
	private CinemachineImpulseSource _cinemachineImpulseSource;
	private float _lastShot = 0f;
	private bool _gamePaused = false;

	private void Start()
	{
		_audioSource = gameObject.AddComponent<AudioSource>();
		_cinemachineImpulseSource = GetComponent<CinemachineImpulseSource>();

		GameManager.instance.onGamePaused += (b) => _gamePaused = b;
	}

	private void Update()
	{
		if (!_gamePaused && _autoFire)
		{
			_lastShot += Time.deltaTime;

			if (_lastShot > 1f / _fireRate)
			{
				_lastShot = 0f;
				Shoot();
			}
		}
	}

	private void Shoot()
	{
		Shake();
		_projectileSpawner.Emit();
		_audioSource.clip = _shootSound;
		_audioSource.Play();
	}

	private void Shake()
	{
		if (_cinemachineImpulseSource != null)
		{
		_cinemachineImpulseSource.GenerateImpulseWithForce(_shakeStrength);

		}
		else
		{
			Debug.LogError("No CinemachineImpulseSource found on weapon!");
		}
	}

	private void OnEnable()
	{
		_projectileSpawner.onCollision += HandleOnProjectileCollision;
	}

	private void OnDisable()
	{
		_projectileSpawner.onCollision -= HandleOnProjectileCollision;
	}

	private void HandleOnProjectileCollision(GameObject other)
	{
		if (other.tag == "Enemy")
		{
			Enemy e = other.GetComponent<Enemy>();
			e.Hit(_damagePerProjectile);
		}
	}
}