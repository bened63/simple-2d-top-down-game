using System;
using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private GameObject _deathFX;
	[SerializeField] private GameObject _spawnOnDeath;
	[SerializeField] private AudioClip _dieSound;
	[SerializeField] private Healthbar _healthbar;
	[SerializeField] private Sensor _sensor;

	[Header("Settings")]
	[SerializeField, Min(0f)] private float _maxHealth;
	[SerializeField, Min(0f)] private float _hitsPerSeconds;
	[SerializeField, Min(0f)] private float _damagePerHit;
	[SerializeField, Min(0f)] private float _speed;
	[SerializeField, Min(0f)] private float _dieDelay;

	[Header("Stats")]
	[SerializeField] private float _dps;

	private Player _player;
	private AudioSource _audioSource;
	private bool _isDying = false;
	private float _health;
	private float _lastDamageTime;

	public Transform target { get; set; }
	public float speed { get => _speed; set => _speed = value; }
	public float health
	{
		get => _health;
		set
		{
			_health = value;
			_healthbar.value = _health;
		}
	}

	// Start is called before the first frame update
	private void Start()
	{
		_audioSource = gameObject.AddComponent<AudioSource>();
		_player = GameObject.FindObjectOfType<Player>();
		target = _player.transform;
		_healthbar.valueMax = _maxHealth;
		health = _maxHealth;
	}

	// Update is called once per frame
	private void Update()
	{
		if (!GameManager.instance.isPaused)
		{
			MoveTo();

			_lastDamageTime += Time.deltaTime;
			if (_lastDamageTime > 1f / _hitsPerSeconds)
			{
				_lastDamageTime = 0;
				GameObject goPlayer = _sensor.IsTagInRange("Player");
				if (goPlayer)
				{
					Player p = goPlayer.GetComponent<Player>();
					p.Hit(_damagePerHit);
				}
			}
		}
	}

	private void OnValidate()
	{
		_dps = _hitsPerSeconds * _damagePerHit;
	}

	private void MoveTo()
	{
		if (target)
		{
			//Toolbox.LookAt2D(transform, target);
			Vector2 dir = _player.transform.position - transform.position;
			transform.Translate(dir.normalized * Time.deltaTime * speed);
		}
	}

	private void StopMoving()
	{
		target = null;
	}

	public void Hit(float damage)
	{
		health -= damage;
		if (health <= 0)
		{
			Die();
		}
	}

	public void Die()
	{
		if (!_isDying)
		{
			_isDying = true;
			StartCoroutine(IEDie(_dieDelay));
		}
	}

	private IEnumerator IEDie(float delay)
	{
		StopMoving();
		AudioSource.PlayClipAtPoint(_dieSound, transform.position);
		yield return new WaitForSeconds(delay);
		Instantiate(_spawnOnDeath, transform.position, Quaternion.identity);
		GameObject fx = Instantiate(_deathFX, transform.position, Quaternion.identity);
		Destroy(fx, 3f);
		Destroy(gameObject);
	}
}