using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class Sensor : MonoBehaviour
{
	[SerializeField, Min(0f)] private float _range;

	private CircleCollider2D _collider;

	public float range
	{
		get => _range;
		set
		{
			_range = value;
			_collider.radius = _range * 3.22f;
		}
	}

	public List<GameObject> objectsInRange { get; set; } = new List<GameObject>();

	private void Awake()
	{
		_collider = GetComponent<CircleCollider2D>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		objectsInRange.Add(collision.gameObject);
	}
	private void OnTriggerExit2D(Collider2D collision)
	{
		objectsInRange.Remove(collision.gameObject);
	}

	private void OnValidate()
	{
		if (!_collider)
		{
			_collider = GetComponent<CircleCollider2D>();
		}
		range = _range;
	}

	public GameObject IsTagInRange(string tag)
	{
		GameObject goTag = objectsInRange.FirstOrDefault(obj => obj.tag == tag);
		return goTag;
	}

	private void OnDrawGizmosSelected()
	{
#if UNITY_EDITOR
		Color col = Color.red;
		col.a = 0.5f;
		UnityEditor.Handles.color = col;
		UnityEditor.Handles.DrawSolidDisc(transform.position, Vector3.forward, range);
#endif
	}
}