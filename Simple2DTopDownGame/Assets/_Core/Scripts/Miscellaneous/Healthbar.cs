using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider), typeof(CanvasGroup))]
public class Healthbar : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private GameObject _icon;
	[SerializeField] private GameObject _background;
	[SerializeField] private GameObject _fill;
	[SerializeField] private GameObject _border;
	[SerializeField] private TextMeshProUGUI _text;

	[Header("Settings")]
	[SerializeField] private string _cameraTag = "MainCamera";
	[SerializeField] private Color _fillColor = Color.red;
	[SerializeField] private float _setSliderLerpTime = 0.1f;
	[SerializeField] private bool _showIcon = false;

	private Slider _slider;
	private CanvasGroup _canvasGroup;
	private float _value;
	private float _valueMax;
	private float _fadeOutTimer = 0.1f;
	private Coroutine _coroutine;
	private Coroutine _fadeCoroutine;

	public float value
	{
		get => _value;
		set
		{
			_value = Mathf.Clamp(value, 0f, valueMax);
			if (_coroutine != null) StopCoroutine(_coroutine);
			_coroutine = StartCoroutine(IESetSlider(_value));
		}
	}

	public float valueMax
	{
		get => _valueMax;
		set
		{
			_valueMax = value;
			_slider.maxValue = _valueMax;
		}
	}

	private void Awake()
	{
		_slider = GetComponent<Slider>();
		_canvasGroup = GetComponent<CanvasGroup>();
	}

	private void OnValidate()
	{
		_fill.GetComponent<Image>().color = _fillColor;
		_icon.SetActive(_showIcon);
	}

	private IEnumerator IESetSlider(float targetValue)
	{
		float lerpTime = _setSliderLerpTime;
		float currentLerpTime = 0f;
		float start = _slider.value;
		float end = targetValue;

		while (currentLerpTime < lerpTime)
		{
			//increment timer once per frame
			currentLerpTime = Mathf.Clamp(currentLerpTime + Time.deltaTime, 0f, lerpTime);

			//lerp!
			float t = currentLerpTime / lerpTime;
			t = Toolbox.Easings.Interpolate(t, Toolbox.Easings.Function.Smoothstep);
			_slider.value = Mathf.LerpUnclamped(start, end, t);
			_text.text = Mathf.RoundToInt(_slider.value).ToString();

			yield return null;
		}
	}

	public void Show()
	{
		_canvasGroup.alpha = 1;
		if (_fadeCoroutine != null) StopCoroutine(_fadeCoroutine);
		_fadeCoroutine = StartCoroutine(IEFade(true));
	}

	public void Hide()
	{
	}

	private IEnumerator IEFade(bool show)
	{
		yield return new WaitForEndOfFrame();
		yield return new WaitForSeconds(_fadeOutTimer);
		_canvasGroup.alpha = 0;

		float lerpTime = 0.1f;
		float currentLerpTime = 0f;
		float start = show ? 0 : 1;
		float end = show ? 1 : 0;

		while (currentLerpTime < lerpTime)
		{
			//increment timer once per frame
			currentLerpTime = Mathf.Clamp(currentLerpTime + Time.deltaTime, 0f, lerpTime);

			//lerp!
			float t = currentLerpTime / lerpTime;
			t = Toolbox.Easings.Interpolate(t, Toolbox.Easings.Function.Smoothstep);
			_slider.value = Mathf.LerpUnclamped(start, end, t);

			yield return null;
		}
	}
}