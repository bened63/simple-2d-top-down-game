using System.Collections;
using UnityEngine;

public class Collectible : MonoBehaviour
{
	[SerializeField] private GameObject _destroyFX;
	[SerializeField, Min(0f)] private float _moveSpeed = 200f;
	[SerializeField, Min(0f)] private float _floatSpeed = 200f;
	[SerializeField, Min(0f)] private float _floatDistance = 0.05f;
	[SerializeField, Min(0f)] private int _xp = 1;

	private GameManager _gameManager;
	private float _angle = 0f;
	private Coroutine _floatingCoroutine;
	private Vector2 _initialPosition;
	private Player _player;
	private bool _isAttracted;

	private void Start()
	{
		_player = FindObjectOfType<Player>();
		_floatingCoroutine = StartCoroutine(IEFloating());

		_gameManager = GameManager.instance;
		_gameManager.collectibleCount++;
		StartCoroutine(IEDieInSeconds(30));
	}

	private void Update()
	{
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			Player p = collision.GetComponent<Player>();
			p.IncreaseXP(_xp);
			Die();
		}
	}

	private void Die()
	{
		GameObject fx = Instantiate(_destroyFX, transform.position, Quaternion.identity);
		Destroy(fx, 0.5f);
		Destroy(gameObject);
	}

	private IEnumerator IEFloating()
	{
		yield return new WaitForEndOfFrame();

		_initialPosition = transform.position;

		while (true)
		{
			UpdateAngle();
			UpdatePosition();
			yield return null;
		}
	}

	private IEnumerator IEMoveTo(Transform target)
	{
		float lerpTime = 0.8f;
		float currentLerpTime = 0f;
		bool run = true;

		Vector2 start = transform.position;
		Vector2 end = target.position;

		while (run)
		{
			//increment timer once per frame
			currentLerpTime += Time.deltaTime;
			if (currentLerpTime > lerpTime)
			{
				currentLerpTime = lerpTime;
			}


			//lerp!
			float t = currentLerpTime / lerpTime;

			float c1 = 0.507f; // 1.70158f;
			float c2 = c1 * 1.525f;

			t = t < 0.5f
				? (Mathf.Pow(2f * t, 2f) * ((c2 + 1f) * 2f * t - c2)) / 2f
				: (Mathf.Pow(2f * t - 2f, 2f) * ((c2 + 1f) * (t * 2f - 2f) + c2) + 2f) / 2f;

			transform.position = Vector3.LerpUnclamped(start, target.position, t);

			//end condition
			run = currentLerpTime < lerpTime;

			yield return null;
		}
	}

	private IEnumerator IEDieInSeconds(float seconds)
	{
		float time = 0f;
		bool run = true;
		while (run)
		{
			yield return new WaitWhile(() => _gameManager.isPaused);
			time += Time.deltaTime;
			run = time < seconds;
			yield return null;
		}
		Die();
	}

	private void UpdateAngle()
	{
		_angle += Time.deltaTime * _floatSpeed;
		if (_angle >= 360f) _angle = 0f;
	}

	private void UpdatePosition()
	{
		float multiplier = Mathf.Sin(Mathf.Deg2Rad * _angle);
		Vector3 pos = _initialPosition;
		pos.y += multiplier * _floatDistance;
		transform.position = pos;
	}

	public void MoveToPlayer()
	{
		if (!_isAttracted)
		{
			_isAttracted = true;
			StopAllCoroutines();
			StartCoroutine(IEMoveTo(_player.transform));
		}
	}
}